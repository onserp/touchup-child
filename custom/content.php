<?php

/**
 * Title
 *
 * filter definition: /inc/title/helper.php
 */
add_filter('touchup_filter_page_title_text', function($title){

    if( is_archive() && is_tax() ){
        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $title = $term->name;
   }

    return $title;
 });

 add_action('onserp_custom_form', function() {
    echo '<h4>Agende uma avaliação gratuita</h4>';
    echo do_shortcode( '[contact-form-7 id="3566" title="cf3"]' );
 });

 add_action('wp_head', function(){
    if ( is_tax() && is_archive() ){ ?>

   <style>
   .term-nossas-tecnologias .qodef-page-title{
      background-image: url(<?php echo wp_get_attachment_url(9796); ?>) !important;
   }
   </style>

<?php
    }
 });
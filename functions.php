<?php

if ( ! function_exists( 'touchup_child_theme_enqueue_scripts' ) ) {
	/**
	 * Function that enqueue theme's child style
	 */
	function touchup_child_theme_enqueue_scripts() {
		$main_style = 'touchup-main';

		wp_enqueue_style( 'touchup-child-style', get_stylesheet_directory_uri() . '/style.css', array( $main_style ) );
	}

	add_action( 'wp_enqueue_scripts', 'touchup_child_theme_enqueue_scripts' );
}

if( !defined('ONSERP_CUSTOM_DIR') ){
	define('ONSERP_CUSTOM_DIR', __DIR__ . DIRECTORY_SEPARATOR . 'custom' . DIRECTORY_SEPARATOR );
}

require_once ONSERP_CUSTOM_DIR . 'content.php';